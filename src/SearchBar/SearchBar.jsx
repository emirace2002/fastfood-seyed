import { BsSearch } from 'react-icons/bs';
import { useState } from "react";


export const SearchBar = ({ searchItems }) => {
	const [value, setValue] = useState('');
	const onSubmit = (e) => {
		e.preventDefault();
		searchItems(value)
	}
	return (

		<form onSubmit={onSubmit} className="search flex-fill d-flex align-items-center" action="">
			<div className="input-group">
				<input className="form-control rounded-pill pe-5 border-success" type="text" placeholder="چی میقولی؟"
					value={value} onChange={event => setValue(event.target.value)} />
				<BsSearch className="position-absolute top-50 translate-middle-y text-muted me-3" />
			</div>
		</form>

	)
}