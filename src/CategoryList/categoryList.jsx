import React, { useEffect, useState } from "react";
import axios from "../axios";
import Loading from "../Loading/loading.jsx";


const CategoryList = ({ filterItems, children }) => {
    const [loading, setLoading] = useState(true);
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        const fetchCategories = async () => {
            const response = await axios.get('/FoodCategory/categories')
            setCategories(response.data);
            setLoading(false);
        }

        fetchCategories()
    }, []);

    const renderContent = () => {
        if (loading) {
            return <Loading />
        }
        return (
            <div className="ps-3 w-100 d-flex align-items-center justify-content-between gap-5">
                <ul className="nav d-flex" onClick={() => filterItems()}>
                    <li className="nav-item">
                        <a className="nav-link" href="#">
                            همه فست فود ها
                        </a>
                    </li>
                    {
                        categories.map(category => (
                            <li className="nav-item" key={category.id} onClick={() => filterItems(category.id)}>
                                <a className="nav-link" href="#">
                                    {category.name}
                                </a>
                            </li>
                        ))
                    }
                </ul>
                {children}
            </div>
        )
    }

    return (
        <nav className="container mt-n5">
            <div className="bg-white rounded-pill border border-success shadow-lg py-3" style={{ height: "80px" }}>
                {renderContent()}
            </div>
        </nav>
    )
}
export default CategoryList;