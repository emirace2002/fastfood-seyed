import './App.css'
import Header from './Header/header.jsx'
import CategoryList from './CategoryList/categoryList.jsx'
import { useEffect, useState } from "react";
import axios from "axios";
import Loading from "./Loading/loading.jsx";
import { FastFoodList } from "./FastFoodList/FastFoodList.jsx";
import { SearchBar } from "./SearchBar/SearchBar.jsx";
import reza from './assets/images/reza.jpg';



function App() {
    const [loading, setLoading] = useState(false);
    const [fastFoodItems, setFastFoods] = useState([]);
    const fetchData = async (categoryId = null) => {
        setLoading(true);
        const response = await axios.get(`https://react-mini-projects-api.classbon.com/FastFood/list/${categoryId ? '?categoryId=' + categoryId : ""}`);
        setLoading(false);
        setFastFoods(response.data);
    };

    useEffect(() => {
        fetchData();
    }, []);

    const filterItems = (categoryId) => {
        fetchData(categoryId);
    }

    const searchItems = async (term) => {
        setLoading(true);
        const response = await axios.get(`https://react-mini-projects-api.classbon.com/FastFood/search/${term ? '?term=' + term : ""}`);
        setLoading(false);
        setFastFoods(response.data);
    }
    const renderContent = () => {
        if (loading) {
            return <Loading theme={"success"} />
        }
        if (fastFoodItems.length === 0) {
            return (
                <>
                    <div className='alert alert-warning text-center '>
                        ازینا نداریم🙈 یه چیز دیگه بقول😊
                    </div>
                    <img className='mx-auto mt-5 d-block rounded-3' src={reza}>

                    </img>
                </>
            )
        }
        else {
            return <FastFoodList fastFoodItems={fastFoodItems} />;
        }
    }

    return (
        <div className='wrapper'>
            <Header />
            <CategoryList filterItems={filterItems}>
                <SearchBar searchItems={searchItems} />
            </CategoryList >
            <div className="container mt-4 ">
                {renderContent()}
            </div>
        </div>
    )
}

export default App
